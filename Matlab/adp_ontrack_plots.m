%% Find new Laps and Sort after min Time
new_lap_longitude = 8.5940;
gnss_pos_new_lap_idxs = [];
for i = 2:length(gnss_pos_latitudes)
    if gnss_pos_longitudes(i-1) > new_lap_longitude && gnss_pos_longitudes(i) <= new_lap_longitude
        gnss_pos_new_lap_idxs = [gnss_pos_new_lap_idxs, i];
    end
end

[~, fastest_lap_idxs] = sort(gnss_pos_timestamps(gnss_pos_new_lap_idxs(2:end)) - gnss_pos_timestamps(gnss_pos_new_lap_idxs(1:end-1)));
lap = fastest_lap_idxs(1);
%% Plot Data
ch002_bgn_idx = find(ch002_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap)), 1) - 1;
ch002_end_idx = find(ch002_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap+1)), 1) - 1;
ch003_bgn_idx = find(ch003_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap)), 1) - 1;
ch003_end_idx = find(ch003_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap+1)), 1) - 1;
gnss_speed_bgn_idx = find(gnss_speed_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap)), 1) - 1;
gnss_speed_end_idx = find(gnss_speed_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap+1)), 1) - 1;
gnss_imu_bgn_idx = find(gnss_imu_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap)), 1) - 1;
gnss_imu_end_idx = find(gnss_imu_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap+1)), 1) - 1;
gnss_attitude_bgn_idx = find(gnss_attitude_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap)), 1) - 1;
gnss_attitude_end_idx = find(gnss_attitude_timestamps >= gnss_pos_timestamps(gnss_pos_new_lap_idxs(lap+1)), 1) - 1;


verbose = true;
figure(11)
%%
plotDataOnTrack(ch002_timestamps, M_b_wishbone_ver_filt, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Gefilterte vertikale Biegung des Querlenkers vorne rechts (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Gefiltertes vertikales Biegemoment am Querlenker M_{b,ver,filt} in Nm')
grid on
print(gcf, '18_ontrack_fastest_lap_M_b_ver', '-dpng', '-r330')

figure(12)
%%
plotDataOnTrack(ch002_timestamps, M_b_wishbone_hor_filt, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Gefilterte horizontale Biegung des Querlenkers vorne rechts (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Gefiltertes horizontales Biegemoment am Querlenker M_{b,hor,filt} in Nm')
grid on
print(gcf, '18_ontrack_fastest_lap_M_b_hor', '-dpng', '-r330')

figure(13)
%%
plotDataOnTrack(ch002_timestamps, M_b_wishbone_abs_filt, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Gefilterte absolute Biegung des Querlenkers vorne rechts (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Gefiltertes absolutes Biegemoment am Querlenker M_{b,abs,filt} in Nm')
grid on
print(gcf, '18_ontrack_fastest_lap_M_b_abs', '-dpng', '-r330')

figure(14)
%%
plotDataOnTrack(ch002_timestamps, 180/pi*M_b_wishbone_ang_filt, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Gefilterter Winkel der Biegung des Querlenkers vorne rechts (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Gefilterter Winkel des Biegungsvektors zur Horizontalen \phi_{b} in °')
zlim([0 360])
zticks(0:90:360)
grid on
print(gcf, '18_ontrack_fastest_lap_M_b_ang', '-dpng', '-r330')

figure(2)
%%
plotDataOnTrack(ch002_timestamps, Delta_F_pushrod_filt, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Gefilterte dynamische Kraft auf den Pushrod vorne links (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Gefilterte dynamische Kraft auf Pushrod \DeltaF_{rod,filt} in N')
grid on
print(gcf, '18_ontrack_fastest_lap_Delta_F_rod', '-dpng', '-r330')

figure(3)
%%
plotDataOnTrack(ch003_timestamps, 1e3*Delta_s_susp_bl, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Federwegsensor hinten links (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Relativer Federweg \Deltas_{spring,rl} in mm')
grid on
print(gcf, '18_ontrack_fastest_lap_Delta_s_spring_rl', '-dpng', '-r330')

figure(4)
%%
plotDataOnTrack(ch003_timestamps, 1e3*Delta_s_susp_br, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Federwegsensor hinten rechts (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Relativer Federweg \Deltas_{spring,rr} in mm')
grid on
print(gcf, '18_ontrack_fastest_lap_Delta_s_spring_rr', '-dpng', '-r330')

figure(4)

%%
plotDataOnTrack(gnss_speed_timestamps, gnss_speeds, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Geschwindigkeit (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Geschwindigkeit v in m/s')
grid on
print(gcf, '18_ontrack_fastest_lap_v', '-dpng', '-r330')

figure(6)
%%
plotDataOnTrack(gnss_attitude_timestamps, gnss_attitude_rolls, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Rollwinkel (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Rollwinkel \phi in °')
grid on
print(gcf, '18_ontrack_fastest_lap_phi', '-dpng', '-r330')

figure(9)
%%
plotDataOnTrack(gnss_attitude_timestamps, psi_dot, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, verbose);
set(gca, "View", [45,45])
title('On-Track: Gierrate (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Gierrate d\psi/dt in °/s')
grid on
print(gcf, '18_ontrack_fastest_lap_psi_dot', '-dpng', '-r330')

figure(10)
%%
plotDataOnTrack(gnss_attitude_timestamps, gnss_attitude_pitches, gnss_pos_latitudes, gnss_pos_longitudes, gnss_pos_timestamps, gnss_pos_new_lap_idxs, lap, true);
set(gca, "View", [45,45])
title('On-Track: Nickwinkel (schnellste Runde)')
xlabel('Relative Fahrzeugposition x in m')
ylabel('Relative Fahrzeugposition y in m')
zlabel('Nickwinkel \theta in °')
grid on
print(gcf, '18_ontrack_fastest_lap_theta', '-dpng', '-r330')
%% Functions

% plot data on track
function plotDataOnTrack(data_timestamps, data_arr, pos_latitude_arr, pos_longitude_arr, timestamp_arr, new_lap_idxs, lap, verbose)

    if verbose
        area_skip_cnt = 0
    else
        area_skip_cnt = round(length(data_arr)*1e-5) % make this bigger if plots are to laggy
    end

    % transform latitude, longitude to local x, y
    mean_altitude = 158; % in m
    origin = [pos_latitude_arr(new_lap_idxs(lap)), pos_longitude_arr(new_lap_idxs(lap)), mean_altitude]; % lat, lon, altitude
    [pos_x_arr, pos_y_arr] = latlon2local(pos_latitude_arr, pos_longitude_arr, mean_altitude, origin);
    
    % plot zero line in form of racetrack
    plot3(pos_x_arr(new_lap_idxs(lap):new_lap_idxs(lap+1)), ...
          pos_y_arr(new_lap_idxs(lap):new_lap_idxs(lap+1)), ...
          zeros(size(pos_x_arr(new_lap_idxs(lap):new_lap_idxs(lap+1)))), ...
          Color=[0, 0, 0])
    hold on
    
    % linear interpolate position between sample points
    interp_xs = zeros(size(data_timestamps));
    interp_ys = zeros(size(data_timestamps));
    data_start_idx = find(data_timestamps >= timestamp_arr(new_lap_idxs(lap)), 1);
    i = data_start_idx;
    cnt = 0;
    while data_timestamps(i) <= timestamp_arr(new_lap_idxs(lap+1))
        cnt = cnt + 1;
        [interp_xs(cnt), interp_ys(cnt)] = linInterpPos(pos_x_arr, pos_y_arr, timestamp_arr, data_timestamps(i));
        i = i + 1;
    end
    interp_xs = interp_xs(1:cnt);
    interp_ys = interp_ys(1:cnt);
    
    data_pp_value = 2 * (max(abs(data_arr(data_start_idx:data_start_idx+cnt-1)))); % pp: peek to peek
    
    % plot data line
    plot3(interp_xs, interp_ys, data_arr(data_start_idx:data_start_idx+cnt-1), ...
          Color=[0 .447 .741]);
    
    
    % reduces number of data points for area (faster)
    if ~verbose
        i = data_start_idx;
        cnt = 0;
        while data_timestamps(i) <= timestamp_arr(new_lap_idxs(lap+1))
            cnt = cnt + 1;
            [interp_xs(cnt), interp_ys(cnt)] = linInterpPos(pos_x_arr, pos_y_arr, timestamp_arr, data_timestamps(i));
            i = i + 1 + area_skip_cnt;
        end
        interp_xs = interp_xs(1:cnt);
        interp_ys = interp_ys(1:cnt);
    end
    
    
    % create regtangles for area between data and track
    xs = zeros(4, cnt-1);
    ys = zeros(4, cnt-1);
    zs = zeros(4, cnt-1);
    cs = zeros(4, cnt-1);
    
    for i = 1:cnt-1
        xs(:,i) = [interp_xs(i); interp_xs(i+1); interp_xs(i+1); interp_xs(i)];
        ys(:,i) = [interp_ys(i); interp_ys(i+1); interp_ys(i+1); interp_ys(i)];
        zs(:,i) = [data_arr(data_start_idx+(i-1)*(1+area_skip_cnt))
                   data_arr(data_start_idx+ i   *(1+area_skip_cnt))
                   0; 0];
        cs(:,i) = [data_arr(data_start_idx+(i-1)*(1+area_skip_cnt)) / data_pp_value + 0.5
                   data_arr(data_start_idx+ i   *(1+area_skip_cnt)) / data_pp_value + 0.5
                   0.5; 0.5];
    end
    
    
    % plot area
    fill3(xs, ys, zs, cs, 'FaceAlpha', 0.5, 'LineStyle', 'none');
    set(gca, "DataAspectRatio", [1 1 data_pp_value/50])
    grid on

end



% linear interpolate position
function [x, y] = linInterpPos(pos_x_arr, pos_y_arr, pos_timestamp_arr, data_timestamp)
    
    pos_timestamp_idx_ge = find(pos_timestamp_arr >= data_timestamp, 1); % ge: greater equal, l: less

    if isempty(pos_timestamp_idx_ge)
        x = pos_x_arr(end);
        y = pos_y_arr(end);
        return
    end
    if pos_timestamp_idx_ge == 1
        x = pos_x_arr(1);
        y = pos_y_arr(1);
        return
    end
    
    time_ge = pos_timestamp_arr(pos_timestamp_idx_ge);
    time_l = pos_timestamp_arr(pos_timestamp_idx_ge - 1);
    
    % t as parameter in [0, 1]
    t = (data_timestamp - time_l)/(time_ge - time_l);

    x_ge = pos_x_arr(pos_timestamp_idx_ge);
    x_l = pos_x_arr(pos_timestamp_idx_ge - 1);
    y_ge = pos_y_arr(pos_timestamp_idx_ge);
    y_l = pos_y_arr(pos_timestamp_idx_ge - 1);

    x = t * (x_ge - x_l) + x_l;
    y = t * (y_ge - y_l) + y_l;
end