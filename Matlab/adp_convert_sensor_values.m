%% Tara ch002 and ch003 Values with Mean of Standstill Values
max_rest_susp_voltage_var = (2e-3)^2;
ch003_time_window = 20; % in s
ch003_smp_freq = 1000; % in Hz
ch003_at_rest_arr = zeros(size(ch003_timestamps)); % bool array

cnt = ch003_time_window*ch003_smp_freq;

% save if car is at_rest in bool array (if variance of suspension voltage in time window is smaller then threshold)
for i = cnt:length(ch003_at_rest_arr)
    ch003_at_rest_arr(i) = var(ch003_susp_l_voltages(i-cnt+1:ch003_smp_freq:i)) < max_rest_susp_voltage_var && ...
                           var(ch003_susp_r_voltages(i-cnt+1:ch003_smp_freq:i)) < max_rest_susp_voltage_var;
end

% find at_rest beginning and end idxs
ch003_at_rest_bgn_idxs = find(diff(ch003_at_rest_arr) == 1); % bgn: beginning
ch003_at_rest_end_idxs = find(diff(ch003_at_rest_arr) == -1) - 1/2 * ch003_time_window * ch003_smp_freq;
ch003_at_rest_end_idxs(end+1) = length(ch003_timestamps);

% remove idxs where standstill time is smaller than threshold time window
i = 1;
while i <= length(ch003_at_rest_bgn_idxs)
    if ch003_timestamps(ch003_at_rest_end_idxs(i)) - ch003_timestamps(ch003_at_rest_bgn_idxs(i)) < ch003_time_window
        ch003_at_rest_bgn_idxs(i) = []; % removes current idx
        ch003_at_rest_end_idxs(i) = []; % removes current idx
        i = i - 1;
    end
    i = i + 1;
end

% figure(3)
% hold on
% plot(ch003_timestamps, ch003_at_rest_arr)
% scatter(ch003_timestamps(ch003_at_rest_bgn_idxs), ones(size(ch003_at_rest_bgn_idxs)));
% scatter(ch003_timestamps(ch003_at_rest_end_idxs), ones(size(ch003_at_rest_bgn_idxs)));
% hold off

% tara voltages in "at_rest" + "moving" timewindows with mean values of "at_rest" timewindows
ch002_dms_z_tar_voltages = zeros(size(ch002_timestamps));
ch002_dms_x_tar_voltages = zeros(size(ch002_timestamps));
ch002_fsens_tar_voltages = zeros(size(ch002_timestamps));

for i = 1:length(ch003_at_rest_bgn_idxs)-1
    ch002_dms_z_tar_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_bgn_idxs(i+1)-1) = ...
        ch002_dms_z_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_bgn_idxs(i+1)-1) - ...
        mean(ch002_dms_z_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_end_idxs(i)));
    ch002_dms_x_tar_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_bgn_idxs(i+1)-1) = ...
        ch002_dms_x_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_bgn_idxs(i+1)-1) - ...
        mean(ch002_dms_x_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_end_idxs(i)));
    ch002_fsens_tar_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_bgn_idxs(i+1)-1) = ...
        ch002_fsens_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_bgn_idxs(i+1)-1) - ...
        mean(ch002_fsens_voltages(ch003_at_rest_bgn_idxs(i):ch003_at_rest_end_idxs(i)));
end

%% Calculate Forces, Bending Moment, Suspension Travel

% use ch002_timestamps for DMS and Force Sensor Values
% use ch003_timestamps for Suspension Travel Sensor Values

U_bridge_source = 5; % in V

U_amp_max = 10; % in V
U_amp_min = -10; % in V
I_amp_max = 20 *1e-3; % in A
I_amp_min = 4 *1e-3; % in A

G_amp = 1 / (0.5); % in V/mv

R_amp = (U_amp_max - U_amp_min) / (I_amp_max - I_amp_min); % in Ohm

R_shunt_dms_z = 501; % in Ohm
R_shunt_dms_x = 502; % in Ohm
R_shunt_fsens = 500; % in Ohm

% Difference (Diagonal) Voltages of Wheatstone Bridges
Delta_U_diff_dms_z = ch002_dms_z_tar_voltages ./ R_shunt_dms_z .* R_amp .* 1 ./ G_amp; % in mV
Delta_U_diff_dms_x = ch002_dms_x_tar_voltages ./ R_shunt_dms_x .* R_amp .* 1 ./ G_amp; % in mV
Delta_U_diff_fsens = ch002_fsens_tar_voltages ./ R_shunt_fsens .* R_amp .* 1 ./ G_amp; % in mV



% DMS

d_wishbone = 15 *1e-3; % in m
E_wishbone = 71 * 1e9; % in Pa
I_wishbone = pi * (d_wishbone/2)^4 / 4; % in m^4

k_dms = 2.06; % in mV/V

nu_wishbone = 0.35;
epsilon_dms_nom = 1/(1+nu_wishbone); % in mm/m bei 1 mV/V

% eps: epsilon, u: under, o: over
eps_u_minus_eps_o = Delta_U_diff_dms_z .* 4 .* epsilon_dms_nom ./ (k_dms .* U_bridge_source) *1e-3; % in m/m
% eps: epsilon, f: front, b: back
eps_b_minus_eps_f = Delta_U_diff_dms_x .* 4 .* epsilon_dms_nom ./ (k_dms .* U_bridge_source) *1e-3; % in m/m

kappa_z = eps_u_minus_eps_o ./ d_wishbone; % in 1/m
kappa_x = eps_b_minus_eps_f ./ d_wishbone; % in 1/m

% ver: vertical
M_b_wishbone_ver = kappa_z .* E_wishbone .* I_wishbone; % in Nm
% hor: horizontal
M_b_wishbone_hor = kappa_x .* E_wishbone .* I_wishbone; % in Nm

% in polar form
M_b_wishbone_abs = sqrt(M_b_wishbone_ver.^2 + M_b_wishbone_hor.^2); % in Nm
M_b_wishbone_ang = atan2(M_b_wishbone_ver, M_b_wishbone_hor); % in rad
% make negative angles positive
for i = 1:length(M_b_wishbone_ang)
    if(M_b_wishbone_ang(i) < 0)
        M_b_wishbone_ang(i) = M_b_wishbone_ang(i) + 2*pi;
    end
end


% FORCE SENSOR

C_fsens_nom = 1; % in mV/V
F_fsens_nom = 5000; % in N bei 1 mV/V

Delta_F_pushrod = Delta_U_diff_fsens .* F_fsens_nom ./ (C_fsens_nom .* U_bridge_source); % in N



% SUSPENSION TRAVEL

l_poti_max = 75 *1e-3; % in m
U_poti_source = 3.3; % in V

% bl: back left, br: back right
% (Delta_ for compatibility. No offset is removed)
% switch polarity for same spring travel direction as dart intern values
Delta_s_susp_bl = (U_poti_source - ch003_susp_l_voltages) / U_poti_source * l_poti_max; % in m
Delta_s_susp_br = (U_poti_source - ch003_susp_r_voltages) / U_poti_source * l_poti_max; % in m





% negate headings once because heading is defined positive in clockwise direction
% also negate pitch
if ~exist('headings_negated','var')
    headings_negated = 1;
    gnss_attitude_headings = 360 - gnss_attitude_headings;
    gnss_attitude_pitches = - gnss_attitude_pitches;
end

% YAW RATE

psi_dot = [0; diff(gnss_attitude_headings)]; % in °/s
% fix angle differences that wrap over 360°
for i = 1:length(psi_dot)
    if psi_dot(i) > 180
        psi_dot(i) = psi_dot(i) - 360;
    end
    if psi_dot(i) < -180
        psi_dot(i) = psi_dot(i) + 360;
    end
end
psi_dot(2:end) = psi_dot(2:end) ./ diff(gnss_attitude_timestamps);

%% Filter DMS Values and Force Sensor Values with Low-Pass Filter
M_b_wishbone_ver_filt = zeros(length(M_b_wishbone_ver),1);
M_b_wishbone_hor_filt = zeros(length(M_b_wishbone_hor),1);
M_b_wishbone_ver_filt(1) = M_b_wishbone_ver(1);
M_b_wishbone_hor_filt(1) = M_b_wishbone_hor(1);

Delta_F_pushrod_filt = zeros(length(Delta_F_pushrod),1);
Delta_F_pushrod_filt(1) = Delta_F_pushrod(1);

N = 10; % cutoff_frequency = sample_frequency / N = 1 kHz / N

for i = 2:length(ch002_dms_z_voltages)
    M_b_wishbone_ver_filt(i) = ( N * M_b_wishbone_ver_filt(i-1) + M_b_wishbone_ver(i) ) / (N + 1);
    M_b_wishbone_hor_filt(i) = ( N * M_b_wishbone_hor_filt(i-1) + M_b_wishbone_hor(i) ) / (N + 1);
    Delta_F_pushrod_filt(i) = ( N * Delta_F_pushrod_filt(i-1) + Delta_F_pushrod(i) ) / (N + 1);
end