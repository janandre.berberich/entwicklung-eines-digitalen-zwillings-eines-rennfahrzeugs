%% Get Data
path = "00000018/00000001.mf4";
finalized_path = mdfFinalize(path);
data = mdfRead(finalized_path);
clear finalized_path
%% Extract Sensor Data

% typecast all byte arrays of data to uint64 and extract, scale and offset relevant bits

ch002_idxs = find(data{44}{:,2} == 002); % DMS_z, DMS_x and 'Kraftsensor' are logged at channel 2 (ch002)
ch003_idxs = find(data{44}{:,2} == 003); % 'Federwegsensoren' are logged at channel 3 (ch003)

ch002_timestamps = zeros(size(ch002_idxs), 'double'); % in s
ch002_dms_z_voltages = zeros(size(ch002_idxs), 'double'); % in V (at front right)
ch002_dms_x_voltages = zeros(size(ch002_idxs), 'double'); % in V (at front right)
ch002_fsens_voltages = zeros(size(ch002_idxs), 'double'); % in V (fsens: Force Sensor) (at front left)
for i = 1:length(ch002_idxs)
    ch002_timestamps(i) = seconds(data{44}(ch002_idxs(i),1).Timestamp);
    ch002_uint64 = typecast(uint8(data{44}{ch002_idxs(i),6}{1}), 'uint64');
    ch002_dms_z_voltages(i) = double(         bitand(ch002_uint64, uint64(                                0b1111111111111111))      ) * 0.625 / 1000; % 0b 16x1 0x0
    ch002_dms_x_voltages(i) = double(bitshift(bitand(ch002_uint64, uint64(                0b11111111111111110000000000000000)), -16)) * 0.625 / 1000; % 0b 16x1 16x0
    ch002_fsens_voltages(i) = double(bitshift(bitand(ch002_uint64, uint64(0b111111111111111100000000000000000000000000000000)), -32)) * 0.625 / 1000; % 0b 16x1 32x0
end

ch003_timestamps = zeros(size(ch003_idxs), 'double'); % in s
ch003_susp_l_voltages = zeros(size(ch003_idxs), 'double'); % in V (susp: suspension, l: left, r: right) (both at the back)
ch003_susp_r_voltages = zeros(size(ch003_idxs), 'double'); % in V
for i = 1:length(ch003_idxs)
    ch003_timestamps(i) = seconds(data{44}(ch003_idxs(i),1).Timestamp);
    ch003_uint64 = typecast(uint8(data{44}{ch003_idxs(i),6}{1}), 'uint64');
    ch003_susp_l_voltages(i) = double(         bitand(ch003_uint64, uint64(                0b1111111111111111))      ) * 0.625 / 2000; % 0b 16x1 0x0
    ch003_susp_r_voltages(i) = double(bitshift(bitand(ch003_uint64, uint64(0b11111111111111110000000000000000)), -16)) * 0.625 / 2000; % 0b 16x1 16x0
end

clear ch002_idxs ch003_idxs ch002_uint64 ch003_uint64
%% Extract GNSS Data

% typecast all byte arrays of data to uint64 and extract, scale and offset relevant bits

% gnss: global navigation satellite system
gnss_status_idxs = find(data{61}{:,2} == 101);
gnss_time_idxs = find(data{61}{:,2} == 102);
gnss_pos_idxs = find(data{61}{:,2} == 103);
gnss_altitude_idxs = find(data{61}{:,2} == 104);
gnss_attitude_idxs = find(data{61}{:,2} == 105);
gnss_distance_idxs = find(data{61}{:,2} == 106);
gnss_speed_idxs = find(data{61}{:,2} == 107);
gnss_imu_idxs = find(data{61}{:,2} == 111);

gnss_status_timestamps = zeros(size(gnss_status_idxs), 'double'); % in s
gnss_status_fixtypes = zeros(size(gnss_status_idxs), 'uint8');
gnss_status_satellites = zeros(size(gnss_status_idxs), 'uint8');
for i = 1:length(gnss_status_idxs)
    gnss_status_timestamps(i) = seconds(data{61}(gnss_status_idxs(i),1).Timestamp);
    gnss_status_uint64 = typecast(uint8(data{61}{gnss_status_idxs(i),6}{1}), 'uint64');
    gnss_status_fixtypes(i) =            bitand(gnss_status_uint64, uint64(     0b111));
    gnss_status_satellites(i) = bitshift(bitand(gnss_status_uint64, uint64(0b11111000)), -3);
end

gnss_time_timestamps = zeros(size(gnss_time_idxs), 'double'); % in s
gnss_time_valids = zeros(size(gnss_time_idxs), 'uint8');
gnss_time_confirmeds = zeros(size(gnss_time_idxs), 'uint8');
gnss_time_epochs = zeros(size(gnss_time_idxs), 'double'); % in s
for i = 1:length(gnss_time_idxs)
    gnss_time_timestamps(i) = seconds(data{61}(gnss_time_idxs(i),1).Timestamp);
    gnss_time_uint64 = typecast(uint8(data{61}{gnss_time_idxs(i),6}{1}), 'uint64');
    gnss_time_valids(i) =                 bitand(gnss_time_uint64, uint64(                                               0b1));
    gnss_time_confirmeds(i) =    bitshift(bitand(gnss_time_uint64, uint64(                                              0b10)), -1);
    gnss_time_epochs(i) = double(bitshift(bitand(gnss_time_uint64, uint64(0b111111111111111111111111111111111111111100000000)), -8)) * 0.001 + 1577840400; % 0b 40x1 8x0
end

gnss_pos_timestamps = zeros(size(gnss_pos_idxs), 'double'); % in s
gnss_pos_valids = zeros(size(gnss_pos_idxs), 'uint8');
gnss_pos_latitudes = zeros(size(gnss_pos_idxs), 'double'); % in °
gnss_pos_longitudes = zeros(size(gnss_pos_idxs), 'double'); % in °
gnss_pos_accuracies = zeros(size(gnss_pos_idxs), 'uint8'); % in m
for i = 1:length(gnss_pos_idxs)
    gnss_pos_timestamps(i) = seconds(data{61}(gnss_pos_idxs(i),1).Timestamp);
    gnss_pos_uint64 = typecast(uint8(data{61}{gnss_pos_idxs(i),6}{1}), 'uint64');
    gnss_pos_valids(i) =                     bitand(gnss_pos_uint64, uint64(                                                               0b1));
    gnss_pos_latitudes(i) =  double(bitshift(bitand(gnss_pos_uint64, uint64(                                   0b11111111111111111111111111110)),  -1)) * 1e-6 -  90; % 0b 28x1 1x0
    gnss_pos_longitudes(i) = double(bitshift(bitand(gnss_pos_uint64, uint64(      0b1111111111111111111111111111100000000000000000000000000000)), -29)) * 1e-6 - 180; % 0b 29x1 29x0
    gnss_pos_accuracies(i) =        bitshift(bitand(gnss_pos_uint64, uint64(0b1111110000000000000000000000000000000000000000000000000000000000)), -58); % 0b 6x1 58x0
end

gnss_altitude_timestamps = zeros(size(gnss_altitude_idxs), 'double'); % in s
gnss_altitude_valids = zeros(size(gnss_altitude_idxs), 'uint8');
gnss_altitudes = zeros(size(gnss_altitude_idxs), 'double'); % in m
gnss_altitude_accuracies = zeros(size(gnss_altitude_idxs), 'uint16'); % in m
for i = 1:length(gnss_altitude_idxs)
    gnss_altitude_timestamps(i) = seconds(data{61}(gnss_altitude_idxs(i),1).Timestamp);
    gnss_altitude_uint64 = typecast(uint8(data{61}{gnss_altitude_idxs(i),6}{1}), 'uint64');
    gnss_altitude_valids(i) =              bitand(gnss_altitude_uint64, uint64(                               0b1));
    gnss_altitudes(i) =    double(bitshift(bitand(gnss_altitude_uint64, uint64(             0b1111111111111111110)),  -1)) * 0.1 - 6000; % 0b 18x1 1x0
    gnss_altitude_accuracies(i) = bitshift(bitand(gnss_altitude_uint64, uint64(0b11111111111110000000000000000000)), -19); % 0b 13x1 19x0
end

gnss_attitude_timestamps = zeros(size(gnss_attitude_idxs), 'double'); % in s
gnss_attitude_valids = zeros(size(gnss_attitude_idxs), 'uint8');
gnss_attitude_rolls = zeros(size(gnss_attitude_idxs), 'double'); % in °
gnss_attitude_roll_accuracies = zeros(size(gnss_attitude_idxs), 'double'); % in °
gnss_attitude_pitches = zeros(size(gnss_attitude_idxs), 'double'); % in °
gnss_attitude_pitch_accuracies = zeros(size(gnss_attitude_idxs), 'double'); % in °
gnss_attitude_headings = zeros(size(gnss_attitude_idxs), 'double'); % in °
gnss_attitude_heading_accuracies = zeros(size(gnss_attitude_idxs), 'double'); % in °
for i = 1:length(gnss_attitude_idxs)
    gnss_attitude_timestamps(i) = seconds(data{61}(gnss_attitude_idxs(i),1).Timestamp);
    gnss_attitude_uint64 = typecast(uint8(data{61}{gnss_attitude_idxs(i),6}{1}), 'uint64');
    gnss_attitude_valids(i) =                             bitand(gnss_attitude_uint64, uint64(                                                               0b1));
    gnss_attitude_rolls(i) =              double(bitshift(bitand(gnss_attitude_uint64, uint64(                                                   0b1111111111110)),  -1)) * 0.1 - 180; % 0b 12x1 1x0
    gnss_attitude_roll_accuracies(i) =    double(bitshift(bitand(gnss_attitude_uint64, uint64(                                          0b1111111110000000000000)), -13)) * 0.1; % 0b 9x1 13x0
    gnss_attitude_pitches(i) =            double(bitshift(bitand(gnss_attitude_uint64, uint64(                              0b1111111111110000000000000000000000)), -22)) * 0.1 - 90; % 0b 12x1 22x0
    gnss_attitude_pitch_accuracies(i) =   double(bitshift(bitand(gnss_attitude_uint64, uint64(                     0b1111111110000000000000000000000000000000000)), -34)) * 0.1; % 0b 9x1 34x0
    gnss_attitude_headings(i) =           double(bitshift(bitand(gnss_attitude_uint64, uint64(         0b1111111111110000000000000000000000000000000000000000000)), -43)) * 0.1; % 0b 12x1 43x0
    gnss_attitude_heading_accuracies(i) = double(bitshift(bitand(gnss_attitude_uint64, uint64(0b1111111110000000000000000000000000000000000000000000000000000000)), -55)) * 0.1; % 0b 9x1 55x0
end

gnss_distance_timestamps = zeros(size(gnss_distance_idxs), 'double'); % in s
gnss_distance_valids = zeros(size(gnss_distance_idxs), 'uint8');
gnss_distance_trip = zeros(size(gnss_distance_idxs), 'uint32'); % in m
for i = 1:length(gnss_distance_idxs)
    gnss_distance_timestamps(i) = seconds(data{61}(gnss_distance_idxs(i),1).Timestamp);
    gnss_distance_uint64 = typecast(uint8(data{61}{gnss_distance_idxs(i),6}{1}), 'uint64');
    gnss_distance_valids(i) =        bitand(gnss_distance_uint64, uint64(                       0b1));
    gnss_distance_trip(i) = bitshift(bitand(gnss_distance_uint64, uint64(0b111111111111111111111110)), -1); % 0b 23x1 1x0
end

gnss_speed_timestamps = zeros(size(gnss_speed_idxs), 'double'); % in s
gnss_speed_valids = zeros(size(gnss_speed_idxs), 'uint8');
gnss_speeds = zeros(size(gnss_speed_idxs), 'double'); % in m/s
gnss_speed_accuracies = zeros(size(gnss_speed_idxs), 'double'); % in m/s
for i = 1:length(gnss_speed_idxs)
    gnss_speed_timestamps(i) = seconds(data{61}(gnss_speed_idxs(i),1).Timestamp);
    gnss_speed_uint64 = typecast(uint8(data{61}{gnss_speed_idxs(i),6}{1}), 'uint64');
    gnss_speed_valids(i) =                     bitand(gnss_speed_uint64, uint64(                                       0b1));
    gnss_speeds(i) =           double(bitshift(bitand(gnss_speed_uint64, uint64(                   0b111111111111111111110)),  -1)) * 0.001; % 0b 20x1 1x0
    gnss_speed_accuracies(i) = double(bitshift(bitand(gnss_speed_uint64, uint64(0b1111111111111111111000000000000000000000)), -21)) * 0.001; % 0b 19x1 21x0
end

gnss_imu_timestamps = zeros(size(gnss_imu_idxs), 'double'); % in s
gnss_imu_valids = zeros(size(gnss_imu_idxs), 'uint8');
gnss_imu_acceleration_xs = zeros(size(gnss_imu_idxs), 'double'); % in m/s^2
gnss_imu_acceleration_ys = zeros(size(gnss_imu_idxs), 'double'); % in m/s^2
gnss_imu_acceleration_zs = zeros(size(gnss_imu_idxs), 'double'); % in m/s^2
gnss_imu_angularrate_xs = zeros(size(gnss_imu_idxs), 'double'); % in °/s
gnss_imu_angularrate_ys = zeros(size(gnss_imu_idxs), 'double'); % in °/s
gnss_imu_angularrate_zs = zeros(size(gnss_imu_idxs), 'double'); % in °/s
for i = 1:length(gnss_imu_idxs)
    gnss_imu_timestamps(i) = seconds(data{61}(gnss_imu_idxs(i),1).Timestamp);
    gnss_imu_uint64 = typecast(uint8(data{61}{gnss_imu_idxs(i),6}{1}), 'uint64');
    gnss_imu_valids(i) =                          bitand(gnss_imu_uint64, uint64(                                                               0b1));
    gnss_imu_acceleration_xs(i) = double(bitshift(bitand(gnss_imu_uint64, uint64                                                     (0b11111111110)),  -1)) * 0.125 - 64; % 0b 10x1 1x0
    gnss_imu_acceleration_ys(i) = double(bitshift(bitand(gnss_imu_uint64, uint64(                                           0b111111111100000000000)), -11)) * 0.125 - 64; % 0b 10x1 11x0
    gnss_imu_acceleration_zs(i) = double(bitshift(bitand(gnss_imu_uint64, uint64(                                 0b1111111111000000000000000000000)), -21)) * 0.125 - 64; % 0b 10x1 21x0
    gnss_imu_angularrate_xs(i) =  double(bitshift(bitand(gnss_imu_uint64, uint64                      (0b111111111110000000000000000000000000000000)), -31)) * 0.25 - 256; % 0b 11x1 31x0
    gnss_imu_angularrate_ys(i) =  double(bitshift(bitand(gnss_imu_uint64, uint64(           0b11111111111000000000000000000000000000000000000000000)), -42)) * 0.25 - 256; % 0b 11x1 42x0
    gnss_imu_angularrate_zs(i) =  double(bitshift(bitand(gnss_imu_uint64, uint64(0b1111111111100000000000000000000000000000000000000000000000000000)), -53)) * 0.25 - 256; % 0b 11x1 53x0
end

clear gnss_status_idxs   gnss_time_idxs   gnss_pos_idxs   gnss_altitude_idxs   gnss_attitude_idxs   gnss_distance_idxs   gnss_speed_idxs   gnss_imu_idxs   ...
      gnss_status_uint64 gnss_time_uint64 gnss_pos_uint64 gnss_altitude_uint64 gnss_attitude_uint64 gnss_distance_uint64 gnss_speed_uint64 gnss_imu_uint64 ...
      i